#!/bin/bash

set -e

while getopts d OPTION "$@"; do
    case $OPTION in
	d)
	    set -x
	    ;;
    esac
done

if [ ! -d node_modules ]; then
    if ! command -v npm &> /dev/null; then
	echo "error: npm not installed"
	exit 1
    fi
    npm install
fi

[ -d css ] || mkdir css

./node_modules/.bin/stylus src/zenburn.styl -o css
./node_modules/.bin/uglifycss css/zenburn.css > css/zenburn.min.css

find css -name '*.css' -print0 |xargs -0 sed -i "s/http:\/\//\/\//g"
